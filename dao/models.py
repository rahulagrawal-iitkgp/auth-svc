from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

Base  = declarative_base()

class User(Base):
    __tablename__ = 'aut_user'
    id  = Column(Integer, primary_key=True, index=True)
    user_name = Column(String, unique = True)
    hashed_password = Column(String)
    email_id = Column(String)
    time_created = Column(DateTime(timezone=True), server_default=func.now())

