from sqlalchemy import select
from dao.models import User
from sqlalchemy.ext.asyncio import AsyncSession
from schema import UserBase

class UserDao:

    @staticmethod
    async def get(user_name, db: AsyncSession):
        statement = select(User).where(User.user_name == user_name)
        user: User = (await db.execute(statement)).scalar()
        return user

    @staticmethod
    async def insert(user: UserBase, db: AsyncSession):
        db_user = User(user_name=user.user_name, hashed_password=user.password, email_id=user.email_id)
        db.add(db_user)
        await db.commit()
        await db.refresh(db_user)
        return db_user
