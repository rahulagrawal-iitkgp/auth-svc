from fastapi import FastAPI, Depends, HTTPException, status
from dao.models import Base
from schema import UserBase, Token
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker, AsyncSession
from config import settings
from utils.oauth import Oauth
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from typing import Annotated
from blogic.user_factory import UserFactory
from dao.user_dao import UserDao

SQLALCHEMY_DATABASE_URL = settings.DATABASE_URL
engine = create_async_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = async_sessionmaker(engine)

async def get_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    db = SessionLocal()
    try:
        yield db
    finally:
        await db.close()


app = FastAPI()
oauth = Oauth(settings.ACCESS_TOKEN_SECRET, settings.ACCESS_TOKEN_TIMEOUT_SEC)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
user_dao = UserDao()
user_factory = UserFactory(user_dao)


@app.get("/test")
async def test(token: Annotated[str, Depends(oauth2_scheme)]):
    if oauth.verify_access_token(token):
        return {"message": "Test Successful"}
    else:
        raise HTTPException(status_code=401, detail="Re-login access token not valid")


@app.post("/register")
async def register(user: UserBase, db: AsyncSession = Depends(get_db)):
    try:
        await user_factory.register_user(user, db)
    except Exception as exc:
        print(exc.args)
        raise HTTPException(status_code=400, detail=str(exc)) from exc



@app.post("/login", response_model=Token)
async def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: AsyncSession = Depends(get_db)):
    user = await user_factory.authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    payload = {'userName': form_data.username}
    access_token = oauth.create_access_token(payload)
    return {"access_token": access_token, "token_type": "bearer"}


