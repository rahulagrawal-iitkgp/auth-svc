import jwt
from datetime import timedelta, datetime, timezone
import copy

class Oauth:
    def __init__(self, access_token_secret, access_token_timeout):
        self.__access_token_secret = access_token_secret
        self.__access_token_timeout = access_token_timeout

    def create_access_token(self, payload):
        payload = copy.copy(payload)
        payload['exp'] = datetime.now(tz=timezone.utc) + timedelta(seconds=int(self.__access_token_timeout))
        return jwt.encode(payload, self.__access_token_secret, algorithm="HS256")

    def verify_access_token(self, access_token):
        try:
            payload = jwt.decode(
                access_token,
                key=self.__access_token_secret,
                algorithms=["HS256"]
            )
            return payload
        except jwt.ExpiredSignatureError as error:
            print(f'Unable to decode the token, error: {error}')
            return None





