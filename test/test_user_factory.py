import unittest
from unittest.mock import MagicMock, ANY, AsyncMock
from dao.models import User
from blogic.user_factory import UserFactory
from dao.user_dao import UserDao
from utils.crypt import Crypt
from schema import UserBase

class TestUserFactory(unittest.IsolatedAsyncioTestCase):

    async def test_authenticate_user_valid_user(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        user = User(user_name="test", hashed_password="testhash", email_id="test@gmail.com")
        userDao.get = AsyncMock(return_value=user)
        Crypt.verify_password = MagicMock(return_value=True)
        ret = userFactory.authenticate_user("test", "test", ANY)
        awaited_ret = await ret
        self.assertEqual(awaited_ret , user, 'User Authentication func for valid user failed')

    async def test_authenticate_user_invalid_user_username(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        userDao.get = AsyncMock(return_value=None)
        ret = userFactory.authenticate_user("test", "test", ANY)
        awaited_ret = await ret
        self.assertEqual(awaited_ret , None, 'User Authentication func for not existing user failed')

    async def test_authenticate_user_invalid_user_password(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        user = User(user_name="test", hashed_password="passhash", email_id="test@gmail.com")
        userDao.get = AsyncMock(return_value=user)
        Crypt.verify_password = MagicMock(return_value=False)
        ret = userFactory.authenticate_user("test", "test", ANY)
        awaited_ret = await ret
        self.assertEqual(awaited_ret , None, 'User Authentication func for not existing user failed')

    async def test_register_user_existing_username(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        user = User(user_name="test", hashed_password="testhash", email_id="test@gmail.com")
        userDao.get = AsyncMock(return_value=user)
        user_base = UserBase(user_name="test", password="test", email_id="test@gmail.com")
        ret = userFactory.register_user(user_base, ANY)
        with self.assertRaises(Exception) as context:
            await ret
        self.assertEqual("Username is already taken", str(context.exception), "User registration func for already existing username failed")

    async def test_register_user_new_user(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        user = User(user_name="test", hashed_password="testhash", email_id="test@gmail.com")
        userDao.get = AsyncMock(return_value=None)
        user_base = UserBase(user_name="test", password="test", email_id="test@gmail.com")
        userDao.insert = AsyncMock(return_value=user)
        Crypt.hash_password = MagicMock(return_value=user.hashed_password)
        ret = userFactory.register_user(user_base, ANY)
        awaited_ret = await ret
        self.assertEqual(awaited_ret.user_name, user.user_name, "User registration func for new user failed")

    async def test_register_user_invalid_email(self):
        userDao = UserDao()
        userFactory = UserFactory(userDao)
        userDao.get = AsyncMock(return_value=None)
        user_base = UserBase(user_name="test", password="test", email_id="testgmail.com")
        ret = userFactory.register_user(user_base, ANY)
        with self.assertRaises(Exception) as context:
            await ret
        self.assertEqual("The email address is not valid. It must have exactly one @-sign.", str(context.exception), "User registration func for invalid email failed")


if __name__ == '__main__':
    unittest.main()