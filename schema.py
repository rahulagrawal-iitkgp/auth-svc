# build a schema using pydantic
from pydantic import BaseModel

class UserBase(BaseModel):
    user_name: str
    password: str
    email_id: str

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str
