# Setting Up Postgres Database
## Installing Postgres Database
```bash
brew install postgresql@15
```

## Starting and Stopping the Postgres Service
```bash
brew services start postgresql@15
brew services stop postgresql@15
```

## Configuring the Postgres Database Server
```bash
psql postgres
CREATE ROLE admin WITH LOGIN PASSWORD 'admin';
ALTER ROLE admin CREATEDB;
psql postgres -U admin;
CREATE DATABASE auth_db;
```

# Setting Up the Auth Service Locally
## Clone the Project to a Local Directory
```bash
git clone git@gitlab.com:rahulagrawal-iitkgp/auth-svc.git
```

## Navigate inside the Auth Project Root Directory
```bash
cd auth-svc
```

## Install All Project Dependencies
```bash
pip3 install -r requirements.txt
```

## Launch the Server
```bash
uvicorn main:app --reload
```

## Launch Swagger to Interact with APIs
Access Swagger at: [http://127.0.0.1:8000/docs#/](http://127.0.0.1:8000/docs#/)

### Note
- Three APIs are available in this prototype: register, login, and test.
- Register API creates a new user.
- Login API is used for login and returns an access_token (refresh_token not implemented for simplicity).
- Test API uses the access_token for subsequent requests until the expiry time (set to 10 seconds for now and can be changed in the .env file).

## To Launch Unit Test Cases trigger following command from root project deirectory
```bash
python3 -m unittest -v
```

## To Launch integration test 

refer - https://gitlab.com/rahulagrawal-iitkgp/auth-svc-functional


