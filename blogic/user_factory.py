from utils.crypt import Crypt
from schema import UserBase
from email_validator import validate_email
from sqlalchemy.ext.asyncio import AsyncSession

class UserFactory:

    def __init__(self, userDao):
        self.__userDao = userDao

    async def authenticate_user(self, username: str, password: str, db: AsyncSession):
        user = await self.__userDao.get(username, db)
        if not user:
            return None
        if not Crypt.verify_password(password, user.hashed_password):
            return None
        return user

    async def register_user(self, user:UserBase, db: AsyncSession):
        validate_email(user.email_id)
        if await self.__verify_unique_username(user.user_name, db):
            hash_password = Crypt.hash_password(user.password)
            user.password = hash_password
            await self.__userDao.insert(user, db)
            return user
        else:
            raise Exception("Username is already taken")

    async def __verify_unique_username(self, username: str, db: AsyncSession):
        user = await self.__userDao.get(username, db)
        if not user:
            return True
        else:
            return False



