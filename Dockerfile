FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11
WORKDIR /new
COPY ./requirements.txt new/requirements.txt
RUN pip install --no-cache-dir --upgrade -r new/requirements.txt
COPY . /new
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]


